'use strict';

//Chat app dependecies
let express = require('express');
let app = express();
let bodyParser = require('body-parser'); // extracts the entire body portion of an incoming request and exposes
let cookieParser = require('cookie-parser');

let session = require('./app/session/session');
let passport = require('./app/auth/authorization');
let methodOverride = require('method-override');


// it on req.body as something easier to interface with
let expressValidator = require('express-validator'); //validate params, query, headers and cookies of request and response an error
let flash = require('connect-flash'); // messsages are written to the flash and being cleared after displayed to the user
let routes = require('./app/routes/index'); // load the router module from index
let users = require('./app/routes/users'); // load the router module from users

let port = process.env.PORT || 3000;

let ioServer = require('./app/socket/socketEvents')(app);

//view engine for jade
app.set("views", "./views");
app.set("view engine", 'pug');

//static folder
app.use(express.static("public"));
app.use(express.static("../node_modules/bootstrap/dist"));
app.use(express.static("node_modules/jquery/dist"));
app.use(express.static("node_modules/js-cookie/src"));
app.use(methodOverride('_method'));
// object req.body contains key value pairs. Value can be anything
// when extended is true or string and array when extended: false
app.use(bodyParser.urlencoded({extended: true}));

//returns middleware that only parses json
app.use(bodyParser.json());

//parses cookie and populate req.cookie
app.use(cookieParser());

app.use(session);
app.use(passport.initialize());
app.use(passport.session());

// The errorFormatter option can be used to specify a function that can be used to format the objects that populate
// the error array that is returned in req.validationErrors()
app.use(expressValidator({
    errorFormatter: (param, msg, value) => {
        let namespace = param.split('.')
            , root = namespace.shift()
            , formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// Connect flash
app.use(flash());

//global lets

app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

app.use('/', routes);
app.use('/users', users);

//noinspection JSUnresolvedFunction
ioServer.listen(port, (err) => {
    if (err) throw err;
    console.log('Server started on port ' + port);
});



module.exports = {
    entry: {
        'gameroom': "./app/client/gameroom.js",
        'table': './app/client/table.js'
    },
    output: {
        path: './public/js',
        filename: "[name].bundle.js"
    },
    watch: true
};
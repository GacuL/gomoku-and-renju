'use strict';

class Model {
    constructor(width, height, cells){
        this.width = width;
        this.height = height;
        this.cells = cells || 15;
        this.boardMargin = this.width * 0.12;
        this.boardMeasure = this.width - this.boardMargin * 2;
        this.cellSize = Math.floor(this.boardMeasure / (this.cells - 1));
        this.boardMeasure = (this.cells - 1) * this.cellSize;

    }
}


module.exports = Model;
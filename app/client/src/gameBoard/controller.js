'use strict';
const $ = require('jquery');

const socketAPI = require('../socketAPI');
let movesArray = [],
    turnNo = 0,
    startGame = $('#butt'),
    socket = '/table';
let gameIsStarted = false;

class Controller {
    constructor(view, model) {
        this.view = view;
        this.model = model;
        this.initBoard();
    }

    initBoard() {
        this.view.drawBoard(this.model);
        this.socketAPI = new socketAPI(socket);
        this.socketAPI.socketEmit('joinOne', tableID);

        this.socketAPI.socketOn('gameIsStarted', () => {
            this.view.onMouseClick((event) => {
                let mousePos = this.getMousePos(event);
                this.socketAPI.socketEmit('makeMove', {mousePos: mousePos, turnNo: turnNo, tableID: tableID});
            }, false);

            this.view.onMouseMove((event) => {
                let mousePos = this.getMousePos(event),
                    asciiPosition = mousePos.x + 97; // translate mouse position to proper letter
                this.view.backlight(mousePos, this.model, '#36b03c');
                if (mousePos) {
                    this.view.getInnerHTML(asciiPosition, mousePos);
                } else {
                    document.getElementById('status').innerHTML = "";
                }

            }, false);
        });

        startGame.on('click', () => {
            gameIsStarted = true;
            this.socketAPI.socketEmit('startGame', tableID);
        });

        this.socketAPI.socketOn('putMove', (data) => {
            if (movesArray.length > 0) {
                this.view.clearLastMove(movesArray[movesArray.length - 1], this.model);
            }
            this.view.makeMove(data.moves, this.model, data.color).showLastMove(data.moves, this.model);
            movesArray.push(data.moves);
        });


    }

    getMousePos(event) {
        let rect = this.view.getBoundingClientRect();
        let xPos = Math.floor((event.clientX - rect.left) / (rect.right - rect.left) * this.model.width) - this.model.boardMargin;
        let yPos = Math.floor((event.clientY - rect.top) / (rect.bottom - rect.top) * this.model.height) - this.model.boardMargin;
        xPos = Math.round(xPos / this.model.cellSize);
        yPos = Math.round((this.model.boardMeasure - yPos) / this.model.cellSize);
        if (xPos >= 0 && yPos >= 0 && xPos < this.model.cells && yPos < this.model.cells) {
            return {
                x: xPos,
                y: yPos
            };
        } else
            return false;
    }
}

module.exports = Controller;

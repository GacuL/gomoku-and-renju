"use strict";

let status = document.getElementById('status'),
    boardMargin;

class Board {
    constructor(canvas0, canvas1, canvas2, canvas3) {
        //canvas1 - topLayer, canvas2 - midLayer, canvas3 - bottomLayer

        this.canvasWidth = canvas3.width;
        boardMargin = this.canvasWidth * 0.12;


        this.context0 = canvas0.getContext('2d');
        this.context0.translate(boardMargin, boardMargin);

        this.context1 = canvas1.getContext('2d');
        this.context1.translate(boardMargin, boardMargin);

        this.context2 = canvas2.getContext('2d');
        this.context2.translate(boardMargin, boardMargin);

        this.context3 = canvas3.getContext('2d');
        this.context3.translate(boardMargin, boardMargin);

        this.context3.lineWidth = 1;
        canvas3.style.background = "#D2B48C";

    }

    drawCircle(x, y, radius, context, color, alpha) {
        context.beginPath();

        context.arc(x, y, radius, 0, Math.PI * 2, true);
        if (typeof color === 'string' || color instanceof String) {
            context.fillStyle = color;
        }
        context.globalAlpha = alpha || 1;
        context.fill();
        context.closePath();
    }


    getBoundingClientRect() {
        return this.context3.canvas.getBoundingClientRect();
    }

    onMouseMove(handler) {
        return this.context1.canvas.addEventListener("mousemove", handler);
    }

    onMouseClick(handler) {
        return this.context1.canvas.addEventListener("click", handler);
    }

    getInnerHTML(asciiPosition, mousePos) {
        return status.innerHTML = String.fromCharCode(asciiPosition) + (mousePos.y + 1);
    }

    drawBoard(model) {
        if (model.width !== model.height) {
            throw 'Width and Height are not the same';
        }

        for (let x = 0; x <= model.boardMeasure; x += model.cellSize) {
            this.context3.moveTo(x, 0);
            this.context3.lineTo(x, model.boardMeasure);
            this.context3.moveTo(0, x);
            this.context3.lineTo(model.boardMeasure, x);
            this.context3.stroke();
        }

        this.drawLandMarks(this.context3, model);
    }

    // drawing orientation points on the board
    drawLandMarks(context, model) {
        let landMark = this.drawCircle;
        landMark(model.boardMeasure / 2, model.boardMeasure / 2, 5, context);
        landMark(model.cellSize * 3, model.cellSize * 3, 5, context);
        landMark(model.boardMeasure - model.cellSize * 3, model.cellSize * 3, 5, context);
        landMark(model.boardMeasure - model.cellSize * 3, model.boardMeasure - model.cellSize * 3, 5, context);
        landMark(model.cellSize * 3, model.boardMeasure - model.cellSize * 3, 5, context);
    }

    //shade under cursor
    backlight(mousePos, model, colorPlayer) {
        let stoneRadius = model.cellSize / 2;// radius of stone

        this.context1.clearRect(-model.cellSize / 2, -model.cellSize / 2, model.boardMeasure + model.cellSize, model.boardMeasure + model.cellSize);

        this.drawCircle(mousePos.x * model.cellSize, model.boardMeasure - mousePos.y * model.cellSize, stoneRadius, this.context1, colorPlayer, 0.5);
    }

    undoMove(currentMove, model) {
        let stoneRadius = model.cellSize / 2;
        this.context2.clearRect((currentMove.x * model.cellSize) - stoneRadius - 0.1, model.boardMeasure - (currentMove.y * model.cellSize) - stoneRadius - 0.1, model.cellSize, model.cellSize);
        return this;
    }

    makeMove(mousePos, model, colorPlayer) {
        let stoneRadius = model.cellSize / 2; // radius of stone
        //call the function that drawing circle
        this.drawCircle(mousePos.x * model.cellSize, model.boardMeasure - mousePos.y * model.cellSize, stoneRadius, this.context2, colorPlayer);
        return this;
    }

    showLastMove(mousePos, model) {

        this.drawCircle(mousePos.x * model.cellSize, model.boardMeasure - mousePos.y * model.cellSize, 4, this.context0, "red");
    }

    clearLastMove(currentMove, model) {
        let stoneRadius = model.cellSize / 2;
        this.context0.clearRect((currentMove.x * model.cellSize) - stoneRadius - 0.1, model.boardMeasure - (currentMove.y * model.cellSize) - stoneRadius - 0.1, model.cellSize, model.cellSize);
    }
}

module.exports = Board;
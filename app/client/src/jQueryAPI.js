'use strict';

function JqueryEvent () {

    function changeLoc(href){
        window.location.href = href;
    }

    this.clickHandler = {
        changeLocation: function(e){
            return changeLoc(e.data.href);
        },

        callback: function (callback){
            return callback;
        },
    };
}

module.exports = JqueryEvent;


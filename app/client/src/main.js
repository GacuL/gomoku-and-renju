'use strict';

const SocketAPI = require('./SocketAPI');
const JqueryEvent = require('./jQueryAPI');
let Model = require('./gameBoard/model');
let Board = require('./gameBoard/board');
let Controller = require('./gameBoard/controller');
// mega wazne do migania!!
// document.addEventListener('visibilitychange', function(){
//     document.title = document.hidden; // change tab text for demo
//
// });


let app = {
    tables: function (tableID) {
        let socket = io('/table'),
            $onTableMessageForm = $('#messageFormTable'),
            $tableChat = $('#table-chat'),
            $tableMessage = $('#tableMessage'),
            $closeTable = $('#closeTable'),
            $minimizeTable = $('#minimizeTable'),
            $player1 = $('#p1'),
            $player2 = $('#p2'),
            event = new JqueryEvent(),
            socketAPI = new SocketAPI(socket);

        let gameBoard = document.getElementById('game-board'),
            canvas0 = document.getElementById('canvas-0'),
            canvas1 = document.getElementById('mid-canvas'),
            canvas2 = document.getElementById('top-canvas'),
            width = gameBoard.attributes.width.value,
            height = gameBoard.attributes.height.value,
            cells = 15;

        let controller = new Controller(new Board(canvas0, canvas2, canvas1, gameBoard), new Model(width, height, cells));

        socketAPI.socketOn('connect', function () {
            console.log('Connection established');

            socketAPI.socketEmit('join', tableID);

            $onTableMessageForm.submit(function (e) {
                e.preventDefault();
                socketAPI.socketEmit('sendMessage', $tableMessage.val());
                $tableMessage.val('');
            });


            $minimizeTable.on('click', {href: '/gameroom'}, event.clickHandler.changeLocation);

            socketAPI.socketOn('newMessage', function (data) {
                $tableChat.append('<b>' + data.username + '</b>' + ": " + data.msg + "</br>");
            });

            $closeTable.on('click', event.clickHandler.callback(function () {
                socketAPI.socketEmit('closeTable', tableID);
            }));

            socketAPI.socketOn('tableIsClosed', function () {
                location.replace('/gameroom');
            });


            $('#p1, #p2').on('click', event.clickHandler.callback(function (e) {
                let seat = e.target.id;
                e.preventDefault();
                socketAPI.socketEmit('getUserName', {tableID: tableID, state: seat});
            }));


            socketAPI.socketOn('userName', function (data) {
                //.prop('disabled', true);
                if (data.state === 'p1') {
                    $player1.text(function (i, text) {
                        //return text === 'player 1' ? data.user : "player 1";
                        if (text === 'player 1') {
                            $player1.prop('disabled', true);
                            return data.user;
                        } else {
                            $player1.prop('disabled', false);
                            return 'player 1'
                        }
                    })
                } else {
                    $player2.text(function (i, text) {
                        if (text === 'player 2') {
                            $player2.prop('disabled', true);
                            return data.user;
                        } else {
                            $player2.prop('disabled', false);
                            return 'player 2'
                        }
                    })
                }
            });

            socketAPI.socketOn('myName', function (data) {
                if ($player1.text === data.user) {
                    $player2.prop('disabled', true);
                } else {
                    $player1.prop('disabled', false);
                }

                if ($player2.text === data.user) {
                    $player2.prop('disabled', true);
                } else {
                    $player2.prop('disabled', false);
                }

            });
        });
    },

    gameRoom: function () {

        let socket = io('/gameroom'),
            messageForm = $('#messageForm'),
            message = $('#message'),
            chat = $('#chat'),
            users = $('#users'),
            tables = $('#tables'),
            tableList = $('#table-list'),
            messages = $('#messages'),
            tableButton = $('#create-table'),
            socketAPI = new SocketAPI(socket),
            event = new JqueryEvent();

        socket.on('connect', function () {
            console.log('Connection established');
        });

        function scrollToBottom() {
            //Selectors
            let newMessage = messages.children('li:last-child'),
                newMessageHeight = newMessage.innerHeight(),
                lastMessageHeight = newMessage.prev().innerHeight();
            //heights

            let clientHeight = messages.prop('clientHeight'),
                scrollTop = messages.prop('scrollTop'),
                scrollHeight = messages.prop('scrollHeight');

            if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
                messages.scrollTop(scrollHeight);
            }
        }

        function changeLocation(data) {
            location.replace('/gameroom/' + data);

        }

        socketAPI.socketOn('tableCreated', function (data) {
            window.location.href = ('/gameroom/' + data.tableID);
            // changeLocation(data.tableID);
        });
//
        socketAPI.socketOn('updateTables', function (data) {
            let div = $('<div></div>');
            data.forEach(function (table) {
                let li = $('<li id="table">' + '<a class="wide" href="/gameroom/' + table.tableID + '">' + '#' + table.tableID + '</a>' + '</li>');
                div.append(li);
            });
            tableList.html(div);
        });

        socketAPI.socketOn('updateUsersList', function (data) {
            let ol = $('<ol class="chat__lists"></ol>');
            data.forEach(function (user) {
                let li = $('<li>' + '<b>' + user + '</b>' + '</li>');
                ol.append(li);
            });
            users.html(ol);
        });

        socketAPI.socketOn('removeTable', function () {
            changeLocation();
        });

        tableButton.on('click', event.clickHandler.callback(function (e) {
            e.preventDefault();
            socket.emit('createTable');
        }));


        messageForm.submit(function (e) {
            e.preventDefault();
            socketAPI.socketEmit('send message', message.val());
            message.val('');
        });

        socketAPI.socketOn('newMessage', function (data) {
            messages.append('<li>' + '<b>' + data.username + '</b>' + ": " + data.msg + '</li>');
            scrollToBottom();
        });
    }
};

module.exports = app;
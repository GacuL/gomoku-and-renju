'use strict';
let io = require('socket.io-client');

class SocketAPI {

    constructor(namespace) {
        this.socket = io(namespace);
        // this.EventType = {
        //     JOIN: "join",
        //     TABLE_CLOSED: "tableClosed"
        // }
    }

    socketEmit(eventName, data) {
        return this.socket.emit(eventName, data);
    };

    socketOn(eventName, handler) {
        return this.socket.on(eventName, handler);
    };
}

module.exports = SocketAPI;





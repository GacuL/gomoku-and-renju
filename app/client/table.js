(function (tableID) {

    const $ = require('jquery');

    const SocketAPI = require('./src/socketAPI');
    const JqueryEvent = require('./src/jQueryAPI');
    const Namespace = require('../common/socketNamespace');
    const EventName = require('../common/socketEventType');

    let Model = require('./src/gameBoard/model');
    let Board = require('./src/gameBoard/board');
    let Controller = require('./src/gameBoard/controller');


    let $onTableMessageForm = $('#messageFormTable'),
        $tableChat = $('#table-chat'),
        $tableMessage = $('#tableMessage'),
        $closeTable = $('#closeTable'),
        $minimizeTable = $('#minimizeTable'),
        $player1 = $('#p1'),
        $player2 = $('#p2');

    let event = new JqueryEvent();
    let socketAPI = new SocketAPI(Namespace.TABLE);

    let gameBoard = document.getElementById('game-board'),
        canvas0 = document.getElementById('canvas-0'),
        canvas1 = document.getElementById('mid-canvas'),
        canvas2 = document.getElementById('top-canvas'),
        width = gameBoard.attributes.width.value,
        height = gameBoard.attributes.height.value,
        cells = 15;

    let controller = new Controller(new Board(canvas0, canvas2, canvas1, gameBoard), new Model(width, height, cells));

    socketAPI.socketOn(EventName.CONNECT, function () {
        console.log('Connection established');
        socketAPI.socketEmit(EventName.JOIN, tableID);

        $onTableMessageForm.submit(function (e) {
            e.preventDefault();
            socketAPI.socketEmit(EventName.SEND_MESSAGE_TABLE, $tableMessage.val());
            $tableMessage.val('');
        });

        $minimizeTable.on('click', {href: '/gameroom'}, event.clickHandler.changeLocation);

        socketAPI.socketOn(EventName.NEW_MESSAGE, function (data) {
            $tableChat.append('<b>' + data.username + '</b>' + ": " + data.msg + "</br>");
        });

        $closeTable.on('click', event.clickHandler.callback(function () {
            socketAPI.socketEmit(EventName.CLOSE_TABLE, tableID);
        }));

        socketAPI.socketOn(EventName.TABLE_IS_CLOSED, function () {
            location.replace('/gameroom');
        });

        $('#p1, #p2').on('click', event.clickHandler.callback(function (e) {
            let seat = e.target.id;
            e.preventDefault();
            socketAPI.socketEmit(EventName.GET_USER_NAME, {tableID: tableID, state: seat});
        }));

        function checkIfEmpty(playerButton1, playerButton2, data){
            if (playerButton1.text() ===data.user && playerButton2.text() === data.user){
                return false;
            } else {
                return true;
            }
        }

        function clickHandler(playerButton1, playerButton2, data) {
                if (playerButton1 === playerButton2) {
                    if (playerButton1.text() !== data.user) {
                        playerButton2.prop('disabled', true);
                        playerButton1.text(data.user);
                    } else {
                        playerButton2.prop('disabled', false);
                        playerButton1.text('player 1');
                    }
                } else {
                    if (playerButton1.text() !== data.user) {
                        playerButton2.prop('disabled', true);
                        playerButton1.text(data.user);
                    } else {
                            playerButton2.prop('disabled', false);
                            playerButton1.text('player 1');

                    }
                }
         }

        socketAPI.socketOn('userName', function (data) {
            if (data.state == 'p1') {
                clickHandler($player1,$player1, data);
            } else {
               clickHandler($player2, $player2, data);
            }
        });

        socketAPI.socketOn('myName', function (data) {
            if (data.state == 'p1') {
                clickHandler($player1,$player2, data);
            } else {
                clickHandler($player2,$player1, data);

            }
        });
    });
})(window.tableID);
(function() {
    const $ = require('jquery');

    const SocketAPI = require('./src/socketAPI');
    const JqueryEvent = require('./src/jQueryAPI');
    const EventName = require('../common/socketEventType');
    const Namespace = require('../common/socketNamespace');
    

    let messageForm = $('#messageForm'),
        message = $('#message'),
        chat = $('#chat'),
        users = $('#users'),
        tables = $('#tables'),
        tableList = $('#table-list'),
        messages = $('#messages'),
        tableButton = $('#create-table'),
        socketAPI = new SocketAPI(Namespace.GAMEROOM),
        event = new JqueryEvent();

    socketAPI.socketOn(EventName.CONNECT, function () {
        console.log('Connection established');
    });

    function scrollToBottom() {
        //Selectors
        let newMessage = messages.children('li:last-child'),
            newMessageHeight = newMessage.innerHeight(),
            lastMessageHeight = newMessage.prev().innerHeight();
        //heights

        let clientHeight = messages.prop('clientHeight'),
            scrollTop = messages.prop('scrollTop'),
            scrollHeight = messages.prop('scrollHeight');

        if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
            messages.scrollTop(scrollHeight);
        }
    }

    function changeLocation(data) {
        location.replace('/gameroom/' + data);
    }

    socketAPI.socketOn(EventName.TABLE_CREATED, function (data) {

              window.location.href = '/gameroom/' + data.tableID;
        // changeLocation(data.tableID);
    });

    socketAPI.socketOn(EventName.UPDATE_TABLES, function (data) {
        let div = $('<div></div>');
        data.forEach(function (table) {
            let li = $('<li id="table">' + '<a class="wide" href="/gameroom/' + table.tableID + '">' + '#' + table.tableID + '</a>' + '</li>');
            div.append(li);
        });
        tableList.html(div);
    });

    socketAPI.socketOn(EventName.UPDATE_USERLIST, function (data) {
        let ol = $('<ol class="chat__lists"></ol>');
        data.forEach(function (user) {
            let li = $('<li>' + '<b>' + user + '</b>' + '</li>');
            ol.append(li);
        });
        users.html(ol);
    });

    socketAPI.socketOn(EventName.REMOVE_TABLE, function () {
        changeLocation();
    });

    tableButton.on('click', event.clickHandler.callback(function (e) {
        e.preventDefault();
        socketAPI.socketEmit(EventName.CREATE_TABLE);
    }));


    messageForm.submit(function (e) {
        e.preventDefault();
        socketAPI.socketEmit(EventName.SEND_MESSAGE_GAMEROOM, message.val());
        message.val('');
    });

    socketAPI.socketOn(EventName.NEW_MESSAGE, function (data) {
        messages.append('<li>' + '<b>' + data.username + '</b>' + ": " + data.msg + '</li>');
        scrollToBottom();
    });
})();
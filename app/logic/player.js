'user strict';

class Player {
    constructor(color) {
        this.color = color;
    }

    static getCurrentPlayer(turnNo){
        if (turnNo % 2 === 0){
            return 'white';
        } else {
            return 'black';
        }
    }

    static getBlackBackground () {
        return 'black';
    }

    static getWhiteBackground () {
        return 'white';
    }
}

module.exports = {Player};
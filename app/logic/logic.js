/**
 * Created by Piotrek-PC on 2017-01-18.
 */
'use strict';

let _ = require('underscore'),
    currentPlayer = {},
    currentColor = {},
    {Player} = require('../logic/player'),
    isBlackTurn;

class Logic {
    constructor() {
        this.movesArray = [];
    }

    checkMove(mousePos, tableID, turnNo) {
        let tempObj = {
            mousePos: mousePos,
            tableID: tableID,
            turnNo: turnNo,
        };

        return new Promise((resolve, reject) => {
            if (_.any(this.movesArray, function (item) {
                    return _.isEqual(item, tempObj);
                }) === false) {
                this.movesArray.push(tempObj);
                resolve({
                    mousePos: mousePos,
                });
            } else {
                return false;
            }
        })
    }

    makeMove(turnNo) {
            currentPlayer = Player.getCurrentPlayer(turnNo);
            currentColor = Player.getWhiteBackground();
            isBlackTurn = true;
            if (currentPlayer === 'white') {
                currentColor = Player.getBlackBackground();
                isBlackTurn = false;
            }
        return {player: currentPlayer, currentColor: currentColor, turnNo: turnNo, turn: isBlackTurn};
    }
}

module.exports = {Logic};
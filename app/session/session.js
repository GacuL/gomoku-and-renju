'use strict';

let session = require("express-session");

let init = () => {
    return session({
        secret: "my-secret",
        resave: true,
        saveUninitialized: true
    });
};
module.exports = init();
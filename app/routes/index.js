let express = require('express'),
    router = express.Router(); // router instance complete middleware and routing system
let User = require('../models/userModel');
let request = require('request');
//get home page

router.get('/', ensureAuthenticated, (req, res) => {
    res.render('index', {title: "Home"});
});

router.get('/gameroom', ensureAuthenticated, (req, res) => {

    res.render('gameroom', {title: "Game Room", username: req.user.username});
});

router.get('/gameroom/:id', ensureAuthenticated, (req, res) => {
    let tableName = 'Table#' + req.params.id;
    res.render('table', {tableID: req.params.id, title: tableName });
});

router.get('/statistics', function(req, res){
    let usr = req.query.uid;
    console.log(usr);
    if (usr !== null) {
        request('/statistics/?uid=' + usr, function () {
            User.getUserByUsername(usr, (err, us) => {
                if (err) {
                    throw err;
                } else {
                    res.render('stats', {data: us});
                }
            });
        });
    }
});



//DELETE ROUTE


//routing to the login page if not logged in
function ensureAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    } else {
        res.redirect('/users/login');
    }
}

module.exports = router;
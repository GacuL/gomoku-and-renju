'use strict';
let express = require('express'),
    router = express.Router(),
    passport = require('../auth/authorization'),
    User = require('../models/userModel');

//register
router.get('/register', (req, res) => {
    res.render('register', {title: "Register"});
});

//login
router.get('/login', (req, res) => {
    res.render('login', {title: "Login"});
});

// Register user
router.post('/register', (req, res) => {

    let name = req.body.name,
        email = req.body.email,
        username = req.body.username,
        password = req.body.password,
        password2 = req.body.password2;

    //validation
    req.check('name', 'Name is required').notEmpty();
    req.check('email', 'Email is required').notEmpty();
    req.check('email', 'Email is not valid').isEmail();
    req.check('username', 'Username is required').notEmpty();
    req.check('password', 'Password is required').notEmpty();
    req.check('password2', 'Passwords do not match').equals(req.body.password);

    let errors = req.validationErrors();

    if (errors) {
        res.render('register', {
            'errors': errors
        });
    } else {
        let newUser = new User({
            name: name,
            email:email,
            username: username,
            password: password,
            rating: 1200
        });

        User.createUser(newUser, (err) => {
            if (err){
                throw err;
            }
        });

        req.flash('success_msg', 'You are registered and can now login');
        res.redirect('/users/login');
    }

});


router.post('/login',
    passport.authenticate('local', {successRedirect: '/', failureRedirect: '/users/login', failureFlash: true}),
    (req, res) => {
        res.redirect('/');
    });

router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/login');
});


router.get('/api/me',
    passport.authenticate('basic', { session: false }),
    function(req, res) {
        res.send(req.user);
    });





module.exports = router;
'use strict';

let sharedsession = require('express-socket.io-session'),
    User = require('../models/userModel'),
    _ = require('underscore'),

    {Users} = require('../models/users'),
    {Logic} = require('../logic/logic'),
    {Tables} = require('../models/tables'),
    EventName = require('../common/socketEventType'),
    Namespace = require('../common/socketNamespace'),
    chat = require('./chat'),

    tables = new Tables(),
    logic = new Logic(),
    users = new Users();

let ioEvents = (io, session) => {

    io.of(Namespace.GAMEROOM).use(sharedsession(session, {
        autoSave: true
    }))
        .on(EventName.CONNECTION, (socket) => {
            console.log('Connection established on the Server');
            socket.emit(EventName.UPDATE_TABLES, tables.getTables());

            if (socket.handshake.session.passport) {
                users.addUserToGameRoom(socket).then((user) => {
                    users.getUsersInGameRoom().then((usersArray) => {
                        io.of(Namespace.GAMEROOM).emit(EventName.UPDATE_USERLIST, usersArray);
                    });
                    chat(user).then((user) => {
                        socket.on(EventName.SEND_MESSAGE_GAMEROOM, (data) => {
                            io.of(Namespace.GAMEROOM).emit(EventName.NEW_MESSAGE, {
                                msg: data,
                                username: user.username
                            });
                        });
                    });
                });
            }

            socket.on(EventName.CREATE_TABLE, () => {
                tables.createTable().then((table) => {
                    tables.removeTableFromArray(table).countPerTable({tableID: table.id, usersIDs: [], moves: []});
                    socket.emit(EventName.TABLE_CREATED, {tableID: table.id});
                    socket.broadcast.emit(EventName.UPDATE_TABLES, tables.getTables());
                });
            });

            socket.on(EventName.DISCONNECT, () => {

                let user = users.removeUserFromGameRoom(socket);

                if (user) {
                    users.getUsersInGameRoom().then((usersArray) => {
                        io.of(Namespace.GAMEROOM).emit(EventName.UPDATE_USERLIST, usersArray);
                    });
                }
            });
        });

    io.of(Namespace.TABLE).use(sharedsession(session, {
        autoSave: true
    }))
        .on(EventName.CONNECTION, (socket) => {
            if (socket.handshake.session.passport) {
                socket.on(EventName.CLOSE_TABLE, (tableID) => {
                    tables.checkUsersOnTable(tableID, socket).closeTable(tableID).then(() => {
                        socket.emit(EventName.TABLE_IS_CLOSED);
                        io.of(Namespace.GAMEROOM).emit(EventName.UPDATE_TABLES, tables.getTables());
                    }).catch(() => {
                        console.log('Table: ' + tableID + ' has new operator');
                        socket.emit(EventName.TABLE_IS_CLOSED);
                    });
                });

                socket.on(EventName.JOIN_GAME, (tableID) => {
                    socket.join(tableID);
                    socket.on(EventName.START_GAME, (tableID) => {

                        (_.any(tables.StartCountPerTable, (item) => {
                            if (item.tableID === tableID && item.usersIDs.length <= 2) {
                                item.usersIDs.push(socket.handshake.session.passport.user);
                                if (item.usersIDs.length === 2) {
                                    io.of(Namespace.TABLE).to(tableID).emit(EventName.GAME_IS_STARTED, {
                                        tableID: item.tableID,
                                    });
                                }
                            }
                        }));
                    });

                    _.any(tables.StartCountPerTable, function (item) {
                        if (item.tableID == tableID) {
                            if (_.contains(item.usersIDs, socket.handshake.session.passport.user)) {
                                console.log('witam');
                                io.of(Namespace.TABLE).to(tableID).emit(EventName.GAME_IS_STARTED, {
                                    tableID: item.tableID,
                                });
                            }
                            socket.emit(EventName.MOVES, item.moves);
                            console.log(item.moves);
                        }
                    });

                    socket.on(EventName.MAKE_MOVE, function (data) {
                        for (let item of tables.StartCountPerTable) {
                            if (item.tableID == data.tableID) {
                                if (_.any(item.moves, (move) => {
                                        return _.isEqual(move, data.mousePos);
                                    }) === false) {
                                    item.moves.push(data.mousePos);
                                    logic.checkMove(data.mousePos, tableID).then((mousePos) => {
                                        let currentPlayer = logic.makeMove(item.moves.length);
                                        io.of(Namespace.TABLE).to(tableID).emit(EventName.PUT_MOVE, {
                                            moves: mousePos.mousePos,
                                            color: currentPlayer.player,
                                        });
                                    });
                                }
                            }
                        }
                    });

                });

                socket.on(EventName.JOIN, (tableID) => {

                    tables.addUser(socket, tableID).then((newUser) => {
                        socket.join(tableID);

                        User.getUserById(newUser.userID, (err, user) => {

                            socket.on(EventName.SEND_MESSAGE_TABLE, (data) => {
                                io.of(Namespace.TABLE).to(tableID).emit(EventName.NEW_MESSAGE, {
                                    msg: data,
                                    username: user.username
                                })
                            });

                            let playerName = {
                                username: user.username
                            };

                            socket.on(EventName.GET_USER_NAME, (data) => {

                                let emitData = function (state) {
                                    socket.broadcast.to(tableID).emit(EventName.USERNAME, {
                                        user: this.username,
                                        state: state
                                    });
                                    socket.emit(EventName.MY_NAME, {user: this.username, state: state});
                                };
                                emitData.call(playerName, data.state);

                            });
                        });
                    });
                });
            }
        });
};

let init = (app) => {

    let session = require('../session/session'),
        server = require('http').Server(app),
        io = require('socket.io')(server);
    io.use(sharedsession(session, {
        autoSave: true
    }));

    ioEvents(io, session);

    return server;
};

module.exports = init;
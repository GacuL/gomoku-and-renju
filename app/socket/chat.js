'use strict';

let User = require('../models/userModel');

function chat(userr) {
    console.log(userr);
    return new Promise((resolve) => {
        User.getUserById(userr, (err, user) => {
            if (err) {
                throw err;
            } else {
                resolve({
                    username: user.username
                })
            }
        });
    });
}

module.exports = chat;


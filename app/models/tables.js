'use strict';
let _ = require('underscore');

class Tables {
    constructor() {
        this.tables = []; //contains created tables
        this.startCountPerTable = []; //array that contains players who pushed start game
        this.removedTabArray = []; //stack for closed tables
    }

    closeTable(tableID) {
        return new Promise((resolve, reject) => {
            for (let item of this.getTables()) {
                if (item.tableID === tableID) {
                    if (item.connections.length === 0) {
                        let removedTable = this.removeTable(tableID);
                        this.removedTabArray.push(removedTable);
                        this.removedTabArray.sort((a, b) => (a - b));
                        resolve();
                    } else {
                        reject();
                    }
                }
            }
        });
    }

    removeTableFromArray(table) {
        let index = this.removedTabArray.indexOf(table.id);
        this.removedTabArray.splice(index, 1);
        return this;
    }

    countPerTable(data) {
        this.startCountPerTable.push(data);
    }

    get StartCountPerTable() {
        return this.startCountPerTable;
    }

    addUser(socket, tableID) {
        return new Promise((resolve) => {
            let userID = socket.handshake.session.passport.user;
            for (let table of this.tables) {
                if (table.tableID === tableID) {
                    if (_.contains(table.connections, userID) === false) {
                        table.connections.push(userID);
                        resolve({id: tableID, userID: userID});
                    } else {
                        resolve({userID: userID});
                    }
                }
            }
        });
    }

    pushTable(tableID){
        this.tables.push({
            tableID: tableID,
            connections: []
        });
    }

    createTable() {
        return new Promise((resolve) => {
            if (this.removedTabArray[0] === undefined) {
                let tableID = this.tables.length + 1;
                if (_.contains(this.tables, tableID) === false) {
                    this.pushTable(tableID);
                    resolve({id: tableID});
                }
            } else {
                this.pushTable(this.removedTabArray[0]);
                resolve({id: this.removedTabArray[0]});
            }
        });
    }

    checkUsersOnTable(tableID, socket) {
        let userID = socket.handshake.session.passport.user;
        _.any(this.tables, (item) => {
            if (item.tableID === tableID) {
                if (_.contains(item.connections, userID)) {
                    let index = item.connections.indexOf(userID);
                    item.connections.splice(index, 1);
                }
            }
        });
       return this;
    }

    removeTable(tableID) {
        let tempID = tableID,
            lowestID = tempID;

        if (lowestID > tempID) {
            lowestID = tempID;
        }
        for (let item of this.tables) {
            if ((item.connections.length) === 0) {
                let index = this.tables.indexOf(item);
                this.tables.splice(index, 1);
                return lowestID;
            }
        }
    }

    getTables() {
        return this.tables;
    }
}

module.exports = {Tables};
'use strict';

let User = require('./userModel'),
    _ = require('underscore');

class Users {
    constructor() {
        this.usersArray = [];
    }

    addUserToGameRoom(socket) {
         return new Promise((resolve) => {
             let id = socket.handshake.session.passport.user;

             if (_.contains(this.usersArray, id) === false){
                 this.usersArray.push(id);
                 console.log(this.usersArray);
                 resolve(id);
             } else {
                 resolve(id);
             }
         });
    }

    removeUserFromGameRoom(socket) {
        let id = socket.handshake.session.passport.user;
        let user = this.getUser(id);
        if (user) {
            this.usersArray = this.usersArray.filter((user)=> user !== id);
        }
        return user;
    }

    getUser(id) {
        return this.usersArray.filter((user) => user === id);
    }


    getUsersInGameRoom() {
        return new Promise((resolve) => {
            let array = [];
            this.usersArray.forEach(function (user, i) {
                User.getUserById(user, (err, user) => {
                    if (err) {
                        throw err;
                    } else {
                        array[i] = user.username;
                        resolve(array);
                    }
                });
            });
        });
    }
}

module.exports = {Users};
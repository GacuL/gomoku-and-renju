'use strict';

let EventName = {
    JOIN: 'join',
    JOIN_GAME: 'joinOne',
    SEND_MESSAGE_TABLE: 'sendMessage',
    SEND_MESSAGE_GAMEROOM: 'send message',
    NEW_MESSAGE: 'newMessage',
    UPDATE_TABLES: 'updateTables',
    UPDATE_USERLIST: 'updateUsersList',
    CREATE_TABLE: 'createTable',
    CLOSE_TABLE: 'closeTable',
    TABLE_IS_CLOSED: 'tableIsClosed',
    GAME_IS_STARTED: 'gameIsStarted',
    MOVES: 'moves',
    MAKE_MOVE: 'makeMove',
    GET_USER_NAME: 'getUserName',
    MY_NAME: 'myName',
    PUT_MOVE: 'putMove',
    CONNECTION: 'connection',
    TABLE_CREATED: 'tableCreated',
    DISCONNECT: 'disconnect',
    START_GAME: 'startGame',
    USERNAME: 'userName',
    CONNECT: 'connect',
    REMOVE_TABLE: 'removeTable'

};

module.exports = EventName;

'use strict';

let Namespace = {
    GAMEROOM: '/gameroom',
    TABLE: '/table'
};

module.exports = Namespace;
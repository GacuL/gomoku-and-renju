const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/gomokuApp'); // connect to the gomokuApp database running locally on port 27017

mongoose.connection.on('error', (err) => {
    if (err) {
        throw err;
    }
});
module.exports = mongoose;

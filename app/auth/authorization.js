'use strict';
let passport = require('passport');
let User = require('../models/userModel');

let LocalStrategy = require('passport-local');

let init = () => {

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });
    passport.deserializeUser((id, done) => {
        User.getUserById(id, (err, user) => {
            done(err, user);
        });
    });

    passport.use(new LocalStrategy(
        (username, password, done) => {
            User.getUserByUsername(username, (err, user) => {
                if (err) throw err;
                if (!user) {
                    return done(null, false, {message: 'unknown user'});
                }

                User.comparePassword(password, user.password, (err, isMatch) => {
                    if (err) throw err;
                    if (isMatch) {
                        return done(null, user);
                    } else {
                        return done(null, false, {message: 'Invalid password'});
                    }
                })
            });
        }
    ));

    return passport;
};

module.exports = init();